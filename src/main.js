var nokia
var rocks
var angle = 0
var pos = 0

var texts = {
  font: null,
  intro: "Nokia phones are indestructible, or so the legend goes. Joking aside, humans developed progressively more sophisticated tools and technologies, colonising and modifying the Earth's surface, calling to life a technostratigraphical period, marked by technofossils. So, what kind of stories could be told by future generations or extraterrestrial species based on only our technofossil evidence?"
}

function preload() {
  nokia = loadModel('obj/nokia.obj')
  rocks = loadModel('obj/rocks.obj')
  texts.font = loadFont('ttf/synemono.ttf')
}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL)
}

function draw() {
  background(255)
  ambientLight(255, 0, 255)
  directionalLight(255, 255, 255, 0, 0, 1)
  // rotation
  push()
  rotateX(Math.PI)
  rotateY(angle * 1.3)
  // nokia
  push()
  scale(1)
  rotateZ(0.4)
  rotateX(-0.6)
  translate(-120, -175, 0)
  noStroke()
  specularMaterial(0, 0, 255)
  model(nokia)
  pop()
  // rocks
  push()
  scale(2.5)
  noStroke()
  translate(0, -100, 0)
  specularMaterial(255, 128, 0)
  model(rocks)
  pop()
  pop()
  // texts
  push()
  textFont(texts.font)
  fill(0, 0, 198)
  textSize(windowWidth * 0.075)
  text(texts.intro, -windowWidth * 0.425, - pos, windowWidth * 0.9)
  pop()
  // rotation
  angle += 0.01
}

function mouseWheel(event) {
  pos += event.delta
}

function windowResized() {
  createCanvas(windowWidth, windowHeight, WEBGL)
}
